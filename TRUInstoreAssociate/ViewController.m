//
//  ViewController.m
//  TRUInstoreAssociate
//
//  Created by Shruti Gupta on 5/2/14.
//  Copyright (c) 2014 Skava. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "CustomerTableViewCell.h"
#define TimeStamp [NSString stringWithFormat:@"%f",[NSDate new]]

@interface ViewController ()
{
    NSString *customerNameIs ;
    NSString *customerLocationIs ;
    NSString *BeaconRecognizedUUID ;
    int MinorCode;//minorcode of the beacon found
    int distanceParm;//distance from the beacon;
    AVAudioPlayer *avSound; //beep sound
}
@property(nonatomic, strong) NSString *BeaconUUID;
@property(strong,nonatomic) CLBeaconRegion *beaconRegion;
@property(strong,nonatomic) CLLocationManager *location;
@property (strong,nonatomic) UITableView *customerServiceRequestTable;
@end

@implementation ViewController
{
    //PROTOTYPE METHODS
    BOOL customerNotificationSent;
    NSString *timeStampOfRequest;
}
- (void)viewDidLoad
{
    //@"7D9E91A9-D30F-41C9-AF15-435CEDBBFE23";//This is the device broadcast UUID
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CustomerInfo" ofType:@"plist"];
    if (path) {
        NSDictionary *root = [NSDictionary dictionaryWithContentsOfFile:path];
        customerNameIs=root[@"customerName"];
        customerLocationIs =root[@"customerLocation"];
        BeaconRecognizedUUID =  root[@"BeaconUUID"];
        
        NSLog(@"customer name is:%@",customerNameIs);
        NSLog(@"customer Location is:%@",customerLocationIs);
    }
    
    
    NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"beep"
                                              withExtension:@"wav"];
    avSound = [[AVAudioPlayer alloc]initWithContentsOfURL:soundURL error:nil];
    
    
    self.location = [[CLLocationManager alloc] init];
    self.location.delegate = self;
    
    
    NSUUID *uuid =[[NSUUID alloc]initWithUUIDString:BeaconRecognizedUUID];
    
    self.beaconRegion =[[CLBeaconRegion alloc]initWithProximityUUID:uuid identifier:@"com.skava"];
    self.beaconRegion.notifyOnEntry=YES;
    self.beaconRegion.notifyOnExit=YES;
    self.beaconRegion.notifyEntryStateOnDisplay=YES;
    [self.location startMonitoringForRegion:self.beaconRegion ];
    
    
    //manual beacon detection
    BOOL enable = [CLLocationManager locationServicesEnabled];
    NSLog(@"%@", enable? @"Enabled" : @"Not Enabled");
    
    [self.location startUpdatingLocation];
    
    [self.view addSubview:self.customerServiceRequestTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma CLLocationManagerDelegate methods
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.location startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.location startRangingBeaconsInRegion:self.beaconRegion];
}
-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Entered" message:@"Beacon region Entered" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [self.location startRangingBeaconsInRegion:self.beaconRegion];
    [avSound play];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


-(void) locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Exited" message:@"Beacon region Exited" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [self.location stopRangingBeaconsInRegion:self.beaconRegion];
}

-(void) locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    //HERE we have found the customer beacon, update the customer table
    
    if (beacons.count > 0)
    {
        customerNotificationSent = YES;
        //capture time of request
        NSDate *now = [NSDate new];
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
        timeFormatter.dateFormat = @"HH:mm:ss";
        timeStampOfRequest = [NSString stringWithFormat:@"%@",[timeFormatter stringFromDate:now]];
        [self.totalCustomersLabel setText:@"1"];
        [self.customerServiceRequestTable reloadData];
    }
        
    CLBeacon *foundBeacon =[beacons firstObject];
    NSString *proximityVal =[NSString stringWithFormat:@"%ld", foundBeacon.proximity];
    
    NSString *addedProximity;
    NSString *distance ;
    switch (foundBeacon.proximity) {
        case CLProximityUnknown:
            addedProximity =[NSString stringWithFormat:@"cannot be located! %li", (long)foundBeacon.rssi];
            distance =@"notSeen";
            break;
        case CLProximityFar:
            addedProximity =[NSString stringWithFormat:@"far from you! %li", (long)foundBeacon.rssi];
            distance =@"far";
            
            break;
        case CLProximityImmediate:
            addedProximity =[NSString stringWithFormat:@"right next to you %li", (long)foundBeacon.rssi];
            //distance =@"middle";
            distance =@"near";
            break;
        case CLProximityNear:
            addedProximity =[NSString stringWithFormat:@"Getting closer! %li", (long)foundBeacon.rssi];
            //distance =@"near";
            distance =@"middle";
            break;
        default:
            addedProximity =[NSString stringWithFormat:@"around here somewhere! %li", (long)foundBeacon.rssi];
            distance =@"far";
            break;
    }
    
}


-(void) locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    NSLog(@"didDetermine state method");
}
-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError method");
}
-(void) locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error
{
    NSLog(@"didFinishDeferredUpdatesWithError method");
}


-(void) locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"RangingBeaconsdidFailForRegion method");
}
-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    NSLog(@"didPauseLocationUpdates method");
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"Couldn't turn on ranging: Location services are not enabled.");
    }
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        NSLog(@"Couldn't turn on monitoring: Location services not authorised.");
    }
}
- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"monitoringDidFailForRegion - error: %@", [error localizedDescription]);
}

#pragma mark - TableView Property
-(UITableView *)customerServiceRequestTable
{
    if (!_customerServiceRequestTable)
    {
        _customerServiceRequestTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 136, 1024, self.view.frame.size.height - 136)];
        [_customerServiceRequestTable setDelegate:self];
        [_customerServiceRequestTable setDataSource:self];
        [_customerServiceRequestTable registerNib:[UINib nibWithNibName:@"CustomerTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CustomerCell"];
        [_customerServiceRequestTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _customerServiceRequestTable;
}

#pragma mark - tableview delegate methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomerTableViewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"CustomerCell"];
    customerNotificationSent = YES;
    
    //capture time of request
    NSDate *now = [NSDate new];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"HH:mm:ss";
    timeStampOfRequest = [NSString stringWithFormat:@"%@",[timeFormatter stringFromDate:now]];
    [self.totalCustomersLabel setText:@"1"];
    [newCell.requestTimeLabel setText:timeStampOfRequest];
    return newCell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (customerNotificationSent)
        return 1;
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (customerNotificationSent)
        return 1;
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 149.0;
}

@end
