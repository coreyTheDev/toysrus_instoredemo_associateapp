//
//  AppDelegate.h
//  TRUInstoreAssociate
//
//  Created by Shruti Gupta on 5/2/14.
//  Copyright (c) 2014 Skava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
