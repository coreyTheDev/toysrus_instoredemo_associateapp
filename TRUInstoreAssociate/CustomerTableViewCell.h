//
//  CustomerTableViewCell.h
//  TRUInstoreAssociate
//
//  Created by CoreysMac on 5/8/14.
//  Copyright (c) 2014 Skava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *elapsedTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *helpCustomerButton;
@property (weak, nonatomic) IBOutlet UIImageView *mapLocationLabel;

@end
