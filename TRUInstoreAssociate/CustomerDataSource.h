//
//  CustomerDataSource.h
//  TRUInstoreAssociate
//
//  Created by CoreysMac on 5/8/14.
//  Copyright (c) 2014 Skava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerDataSource : NSObject
@property (nonatomic, strong) UIImage *customerImage;
@property (nonatomic, strong) UIImage *customerLocationImage;
@property (nonatomic, strong) NSString *customerName;
@property (nonatomic, strong) NSString *aisleName;
@end
