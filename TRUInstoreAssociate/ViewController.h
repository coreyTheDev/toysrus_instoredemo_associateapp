//
//  ViewController.h
//  TRUInstoreAssociate
//
//  Created by Shruti Gupta on 5/2/14.
//  Copyright (c) 2014 Skava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *totalCustomersLabel;

@end
